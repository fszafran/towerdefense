﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TurretUpgradePanel : MonoBehaviour
{
    public MaterialManager matsManager;
    public GameObject panelReference;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI damageText;
    public TextMeshProUGUI rangeText;
    public TextMeshProUGUI ttsText;

    [SerializeField] private Button closePanelButton;
    private TurretFiring currentTurret;

    delegate bool PanelStatusDelegate();
    PanelStatusDelegate panelEvent;

    private void Start()
    {
        matsManager = MaterialManager.instance;
    }

    private void Update()
    {
        closePanelButton.onClick.AddListener(ClosePanel);
    }

    public void SetNewCurrentTurret(TurretFiring newCurrentTurret, PanelStatus newPanelStatus = PanelStatus.Closed)
    {
        currentTurret = newCurrentTurret;
        UpdateDisplayedTexts();
        if (newPanelStatus == PanelStatus.Open)
        {
            OpenPanel();
        }
    }

    public void OpenPanel()
    {
        panelReference.SetActive(true); 
    }

    public void ClosePanel()
    {
        panelReference.SetActive(false);
    }

    private void SetPanelStatus(PanelStatusDelegate panelDelegate)
    {
        panelReference.SetActive(panelDelegate());
    }

    public bool CheckIfOpen()
    {
        return panelReference.activeInHierarchy;
    }

    public void UpdateDisplayedTexts()
    {
        damageText.text = currentTurret.GetCurrentDamage().ToString();
        rangeText.text = currentTurret.GetCurrentRange().ToString();
        ttsText.text = currentTurret.GetCurrentTTS().ToString();
        nameText.text = currentTurret.turretObject.turretName;
    }

    public void TryUpgradeTurretDamage(int upgradeValue)
    {
        if (matsManager.Pay(currentTurret.turretObject.damageUpgradePrice))
        {
            currentTurret.UpgradeTurretDamage(upgradeValue);
            UpdateDisplayedTexts();
        }
    }

    public void TryUpgradeTurretRange(float upgradeValue)
    {
        if (matsManager.Pay(currentTurret.turretObject.rangeUpgradePrice))
        {
            currentTurret.UpgradeTurretRange(upgradeValue);
            UpdateDisplayedTexts();
        }
    }

    public void TryUpgradeTurretTTS(float upgradeValue)
    {
        if (matsManager.Pay(currentTurret.turretObject.ttsUpgradePrice))
        {
            currentTurret.UpgradeTurretTTS(upgradeValue);
            UpdateDisplayedTexts();
        }
    }
}
