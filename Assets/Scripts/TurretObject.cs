﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[CreateAssetMenu(fileName = "New Turret Object", menuName = "Turret")]
public class TurretObject : ScriptableObject
{
    [Min(0)]
    public int turetDamage;
    public float turretRange;
    public float timeToShoot;
    public string turretName;
    public Sprite turretImage;
    public GameObject turretModel;
    public List<Price> placePrices = new List<Price>();
    public Price damageUpgradePrice;
    public Price rangeUpgradePrice;
    public Price ttsUpgradePrice;
}

[System.Serializable]
public struct Price
{
    public MaterialType materialType;
    public int cost;
}