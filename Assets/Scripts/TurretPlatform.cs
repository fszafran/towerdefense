﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretPlatform : MonoBehaviour
{
    public Color defPlatformColor;

    private bool hasTurretPlaced;
    private GameObject turretOwned;

    private void Start()
    {
        defPlatformColor = GetComponent<Renderer>().material.color;
    }

    public void SetToDefaultColor()
    {
        GetComponent<Renderer>().material.color = defPlatformColor;
    }

    public void SetNewDefaultColor(Color newColor)
    {
        defPlatformColor = newColor;
    }

    public void SetTemporaryColor(Color tempColor)
    {
        GetComponent<Renderer>().material.color = tempColor;
    }

    public void PlaceTurret(GameObject newTurret)
    {
        hasTurretPlaced = true;
        turretOwned = newTurret;
    }

    public void RemoveAndDestoryOwnedTurret()
    {
        Destroy(turretOwned);
        hasTurretPlaced = false;
    }

    public TurretFiring GetOwnedTurret()
    {
        return turretOwned.GetComponent<TurretFiring>();
    }

    public bool CheckForTurret()
    {
        return hasTurretPlaced;
    }
}
