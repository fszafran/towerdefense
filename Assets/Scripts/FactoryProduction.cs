﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryProduction : MonoBehaviour
{
    public int productionRate;
    public List<MaterialProduct> producedMaterials = new List<MaterialProduct>();

    private float timeSinceLastCheck;

    void Start()
    {
        timeSinceLastCheck = 0f;
    }

    void Update()
    {
        if(timeSinceLastCheck >= 1f)
        {
            for (int i = 0; i < producedMaterials.Count; i++)
            {
                if (Random.Range(0, 100) < producedMaterials[i].productionChance)
                    producedMaterials[i].materialProduced.amount += productionRate;
            }
            timeSinceLastCheck = timeSinceLastCheck - 1;
        }
        timeSinceLastCheck += Time.deltaTime;
    }
    public void UpgradeProductionRate(int upgradeValue)
    {
        productionRate += upgradeValue;
    }
}

[System.Serializable]
public class MaterialProduct
{
    public MaterialObject materialProduced;
    [Range(0, 100)]
    public int productionChance;
}