﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public bool allowMovement = true;
    public int direction = 0;
    public int stoppingDistance;
    public int healthPoints;

    private bool attackWall;
    private bool checkForEnemy = true;
    private float currentSpeed;
    private WallHealth wallHealth;
    [SerializeField] private Slider healthSlider;
    [SerializeField] private EnemyObject enemyObject;
    
    private void Start()
    {
        currentSpeed = enemyObject.movementSpeed;
        healthPoints = enemyObject.startingHealth;
    }

    private void Update()
    {
        if (checkForEnemy)
        {
            CheckIfColldesWithEnemy();
        }
        if (allowMovement)
        {
            Move();
        }
    }

    private void Move()
    {
        if (direction == 0)
        {
            transform.position = new Vector3(transform.position.x - 2 * Time.deltaTime * currentSpeed, transform.position.y, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 2 * direction * Time.deltaTime * currentSpeed);
        }
        if (currentSpeed < enemyObject.movementSpeed)
        {
            currentSpeed += .5f * Time.deltaTime;
        }
    }

    public void TakeDamage(int damageTaken)
    {
        healthPoints -= damageTaken;
        healthSlider.value = (float)healthPoints / (float)enemyObject.startingHealth;
    }

    public void CheckIfColldesWithEnemy()
    {
        RaycastHit hit;
        Debug.DrawRay(transform.position, new Vector3(1, -1, 0));
        if (direction == 0)
        {
            if (Physics.Raycast(transform.position, new Vector3(-1, 0, 0), out hit, stoppingDistance))
            {
                if (hit.collider.tag == "EnemyCollider")
                {
                    StopMovement();
                }
            }
            else
            {
                StartMovement();
            }
        }
        else
        {
            if (Physics.Raycast(transform.position, new Vector3(0, 0, direction), out hit, stoppingDistance))
            {
                if (hit.collider.tag == "EnemyCollider")
                {
                    StopMovement();
                }
            }
            else
            {
                StartMovement();
            }
        }
    }


    private IEnumerator DamageWall()
    {
        while(true)
        {
            wallHealth.TakeDamage(enemyObject.damage);
            yield return new WaitForSeconds(enemyObject.timeToAttack);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Wall"))
        {
            StartWallAttack(other.GetComponent<WallHealth>());
        }
    }

    private void StopMovement()
    {
        allowMovement = false;
        currentSpeed = 0;
        //frontCollider.radius = 0.5f;
    }

    private void StartWallAttack(WallHealth inWallHealth)
    {
        StopMovement();
        attackWall = true;
        wallHealth = inWallHealth;
        checkForEnemy = false;
        StartCoroutine(DamageWall());
    }

    private void StartMovement()
    {
        allowMovement = true;
        //frontCollider.radius = 0.25f;
    }
}
