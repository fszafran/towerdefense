﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretFiring : MonoBehaviour
{
    public TurretObject turretObject;

    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private SpawnEnemies spawnScript;
    private int turretDamage;
    private float turretRange;
    private float turretTimeToShoot;
    private GameObject closestEnemy = null;

    private void Start()
    {
        turretDamage = turretObject.turetDamage;
        turretRange = turretObject.turretRange;
        turretTimeToShoot = turretObject.timeToShoot;
        StartCoroutine(AttemptToFire());
        spawnScript = SpawnEnemies.Instance;
    }

    public void UpgradeTurretDamage(int upgradeValue)
    {
        turretDamage += upgradeValue;
    }

    public void UpgradeTurretRange(float upgradeValue)
    {
        turretRange += upgradeValue;
    }

    public void UpgradeTurretTTS(float upgradeValue)
    {
        turretTimeToShoot -= upgradeValue;
    }

    public int GetCurrentDamage() { return turretDamage; }

    public float GetCurrentRange() { return turretRange; }

    public float GetCurrentTTS() { return turretTimeToShoot; }

    private IEnumerator AttemptToFire()
    {
        while(true)
        {
            FireAtClosestEnemy();
            yield return new WaitForSeconds(turretTimeToShoot);
        }
    }

    private void FireAtClosestEnemy()
    {
        if (FindClosestEnemy())
        {
            Vector3 closestEnemyPosition = closestEnemy.transform.position;
            if (CheckIfEnemyInRange())
            {
                GameObject Projectile = Instantiate(projectilePrefab, transform);
                Projectile.GetComponent<Projectile>().SetTargetPosition(closestEnemyPosition);
                Projectile.GetComponent<Projectile>().SetDamage(turretDamage);
            }
        }
    }

    private bool FindClosestEnemy()
    {
        if (spawnScript.enemies.Count > 0)
        {
            closestEnemy = spawnScript.enemies[0];
        }
        else
        {
            return false;
        }
        for(int i = 0; i<spawnScript.enemies.Count; i++)
        {
            if((spawnScript.enemies[i].transform.position - transform.position).magnitude < (closestEnemy.transform.position - transform.position).magnitude)
            {
                closestEnemy = spawnScript.enemies[i];
            }
        }
        return true;
    }

    private bool CheckIfEnemyInRange()
    {
        if ((closestEnemy.transform.position - transform.position).magnitude < turretRange)
        {
            return true;
        }
        return false;
    }
}
