﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class MaterialHUD : MonoBehaviour
{
    public MaterialObject mat;

    private TextMeshProUGUI materialDisplayText;
    private Image materialDisplayImage;

    void Start()
    {
        materialDisplayText = GetComponentInChildren<TextMeshProUGUI>();
        materialDisplayImage = GetComponentInChildren<Image>();
    }

    void Update()
    {
        materialDisplayText.text = mat.amount.ToString();
        materialDisplayImage.sprite = mat.materialIcon;
    }
}
