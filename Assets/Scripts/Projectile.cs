﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float projectileSpeed;
    public float distanceToDestroy;

    private int damage;
    private Vector3 vectorToTarget;
    private Vector3 startPosition;

    private void Start()
    {
        startPosition = transform.position;
    }

    private void Update()
    {
        transform.position = transform.position + vectorToTarget * projectileSpeed * Time.deltaTime;
        if( GetDistanceFromStart() >= distanceToDestroy )
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Enemy"))
            col.GetComponent<Enemy>().TakeDamage(damage);
    }

    public void SetTargetPosition(Vector3 newTargetPosition)
    {
        Vector3 direction = newTargetPosition - transform.position;
        vectorToTarget = direction / direction.magnitude;
    }

    public void SetDamage(int newDamage)
    {
        damage = newDamage;
    }

    private float GetDistanceFromStart()
    {
        return Vector3.Distance(startPosition, transform.position);
    }

}
