﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemScript : MonoBehaviour
{
    public TurretObject turretObj;
    [SerializeField] private Image shopIcon;
    [SerializeField] private GameObject pricePanelPrefab;
    [SerializeField] private GameObject slotsTransform;

    private List<GameObject> PricePanels = new List<GameObject>();

    void Start()
    {
        CreateShopSlot();
    }

    public void ShowPrices()
    {
        for (int i = 0; i < PricePanels.Count; i++)
        {
            PricePanels[i].SetActive(true);
        }
    }
    public void HidePrices()
    {
        for(int i = 0; i<PricePanels.Count; i++)
        {
            PricePanels[i].SetActive(false);
        }
    }
    private void CreateShopSlot()
    {
        shopIcon.sprite = turretObj.turretImage;
        MaterialManager MatManager = MaterialManager.instance;
        for (int i = 0; i < turretObj.placePrices.Count; i++)
        {
            //Add widgets for proper prices
            if (turretObj.placePrices[i].cost > 0)
            {
                GameObject PricePanel = Instantiate(pricePanelPrefab, slotsTransform.transform);
                PPContainer ppContainer = PricePanel.GetComponent<PPContainer>();
                ppContainer.textReference.text = turretObj.placePrices[i].cost.ToString();
                for (int g = 0; g < MatManager.mats.Count; g++)
                {
                    if (MatManager.mats[g].materialType == turretObj.placePrices[i].materialType)
                        ppContainer.iconImageReference.sprite = MatManager.mats[g].materialIcon;
                }
                PricePanels.Add(PricePanel);
            }
        }
        HidePrices();
    }
}

[System.Serializable]
public class PriceText
{
    public MaterialType materialName;
    public TextMeshProUGUI textReference;
    public GameObject pricePanel;
}