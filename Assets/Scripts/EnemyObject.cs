﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewEnemy",menuName ="Enemy Object")]
public class EnemyObject : ScriptableObject
{
    public int startingHealth;
    public int damage;
    public int movementSpeed;
    public float timeToAttack;
}
