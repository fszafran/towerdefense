﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum PanelStatus
{
    Open,
    Closed
}

public class FactoryUpgradePanel : MonoBehaviour
{
    public GameObject panelReference;
    public TextMeshProUGUI rateText;
    public TextMeshProUGUI nameText;

    private FactoryProduction currentFactory;

    public void UpdateCurrentFactory(FactoryProduction newCurrentFactory, PanelStatus newPanelStatus = PanelStatus.Closed)
    {
        currentFactory = newCurrentFactory;
        UpdateDisplayedTexts();
        if(newPanelStatus == PanelStatus.Open)
            OpenPanel();
    }
    public void OpenPanel()
    {
        panelReference.SetActive(true);
    }
    public void ClosePanel()
    {
        panelReference.SetActive(false);
    }
    public void UpgradeProductionRate(int upgradeValue)
    {
        currentFactory.UpgradeProductionRate(upgradeValue);
        UpdateDisplayedTexts();
    }
    public bool CheckIfOpen()
    {
        return panelReference.activeInHierarchy;
    }
    private void UpdateDisplayedTexts()
    {
        rateText.text = currentFactory.productionRate.ToString();
        nameText.text = currentFactory.name;
    }
}
