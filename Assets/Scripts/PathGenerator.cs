﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class PathGenerator : MonoBehaviour
{
    public List<GameObject> path = new List<GameObject>();
    public List<GameObject> platforms = new List<GameObject>();
    Stopwatch stop;

    [SerializeField] private int desiredPoint;
    [SerializeField] public Vector2 gridSize;
    [SerializeField] private GameObject floorPrefab;
    [SerializeField] private GameObject platformPrefab;
    [SerializeField] private GameObject waypointPrefab;

    private void Awake()
    {
        //GenerateTenPoints();
        stop = new Stopwatch();
        stop.Start();
        //Generate();
        GeneratePath();
        stop.Stop();
        UnityEngine.Debug.Log(stop.ElapsedMilliseconds);
    }

    private void Generate()
    {
        int moves = 1;
        int direction = 0;
        int temp_direction = 0;
        List<int> directions = new List<int>();

        while (true)
        {
            path.Add(Instantiate(floorPrefab));
            path.Add(Instantiate(floorPrefab));
            //Generate the path until it reaches the desired point
            while (path[path.Count - 1].transform.position.x < desiredPoint)
            {
                GameObject temp = Instantiate(floorPrefab, path[path.Count - 1].transform.position, Quaternion.identity, transform);
                while (true)
                {
                    if (moves % 3 == 0)
                        direction = Random.Range(0, 50);
                    if (direction < 5)
                    {
                        temp_direction = 0;
                        temp.transform.position = new Vector3(path[path.Count - 1].transform.position.x + floorPrefab.GetComponent<MeshRenderer>().bounds.size.x, 0, path[path.Count - 1].transform.position.z);

                    }
                    else if (direction > 5 && direction < 25)
                    {
                        temp_direction = -1;
                        temp.transform.position = new Vector3(path[path.Count - 1].transform.position.x, 0, path[path.Count - 1].transform.position.z + floorPrefab.GetComponent<MeshRenderer>().bounds.size.z);
                    }
                    else if (direction >= 25 && direction <= 50)
                    {
                        temp_direction = 1;
                        temp.transform.position = new Vector3(path[path.Count - 1].transform.position.x, 0, path[path.Count - 1].transform.position.z - floorPrefab.GetComponent<MeshRenderer>().bounds.size.z);
                    }
                    if (temp.transform.position == path[path.Count - 2].transform.position || temp.transform.position == path[path.Count - 1].transform.position || temp.transform.position.z < -14 || temp.transform.position.z > 14)
                    {
                        moves++;
                        continue;
                    }
                    break;
                }
                directions.Add(temp_direction);
                path.Add(temp);
                moves++;
            }
            //Check if the built path is long enough
            //if not destroy the path and generate a new one
            if (path.Count < 160 || path.Count > 170) //TOOD: magic number, fix possible endless re-generations
            {
                for (int i = 0; i < path.Count; i++)
                {
                    Destroy(path[i]);
                }
                path.Clear();
                directions.Clear();
                continue;
            }
            //if the path is correct, add waypoints and side platforms to it
            for (int i = 0; i < path.Count - 2; i++)
            {
                GameObject temp_waypoint = Instantiate(waypointPrefab, path[i + 2].transform.position, Quaternion.identity, path[i + 2].transform);
                temp_waypoint.transform.localScale = new Vector3(0.1f, 1, 0.1f);

                GameObject FloorPlace = Instantiate(platformPrefab, path[i].transform.position, Quaternion.identity, transform);
                GameObject FloorPlace2 = Instantiate(platformPrefab, path[i].transform.position, Quaternion.identity, transform);
                FloorPlace.transform.position = new Vector3(path[i + 1].transform.position.x, 0, path[i + 1].transform.position.z - floorPrefab.GetComponent<Renderer>().bounds.size.z);
                FloorPlace2.transform.position = new Vector3(path[i + 1].transform.position.x, 0, path[i + 1].transform.position.z + floorPrefab.GetComponent<Renderer>().bounds.size.z);
                platforms.Add(FloorPlace);
                platforms.Add(FloorPlace2);

                FloorPlace = Instantiate(platformPrefab, path[i].transform.position, Quaternion.identity, transform);
                FloorPlace2 = Instantiate(platformPrefab, path[i].transform.position, Quaternion.identity, transform);
                FloorPlace.transform.position = new Vector3(path[i + 1].transform.position.x + floorPrefab.GetComponent<Renderer>().bounds.size.x, 0, path[i + 1].transform.position.z);
                FloorPlace2.transform.position = new Vector3(path[i + 1].transform.position.x - floorPrefab.GetComponent<Renderer>().bounds.size.x, 0, path[i + 1].transform.position.z);
                platforms.Add(FloorPlace);
                platforms.Add(FloorPlace2);

                FloorPlace = Instantiate(platformPrefab, path[i].transform.position, Quaternion.identity, transform);
                FloorPlace2 = Instantiate(platformPrefab, path[i].transform.position, Quaternion.identity, transform);
                FloorPlace.transform.position = new Vector3(path[i + 1].transform.position.x + floorPrefab.GetComponent<Renderer>().bounds.size.x, 0, path[i + 1].transform.position.z + floorPrefab.GetComponent<Renderer>().bounds.size.z);
                FloorPlace2.transform.position = new Vector3(path[i + 1].transform.position.x - floorPrefab.GetComponent<Renderer>().bounds.size.x, 0, path[i + 1].transform.position.z + floorPrefab.GetComponent<Renderer>().bounds.size.z);
                platforms.Add(FloorPlace);
                platforms.Add(FloorPlace2);

                FloorPlace = Instantiate(platformPrefab, path[i].transform.position, Quaternion.identity, transform);
                FloorPlace2 = Instantiate(platformPrefab, path[i].transform.position, Quaternion.identity, transform);
                FloorPlace.transform.position = new Vector3(path[i + 1].transform.position.x + floorPrefab.GetComponent<Renderer>().bounds.size.x, 0, path[i + 1].transform.position.z - floorPrefab.GetComponent<Renderer>().bounds.size.z);
                FloorPlace2.transform.position = new Vector3(path[i + 1].transform.position.x - floorPrefab.GetComponent<Renderer>().bounds.size.x, 0, path[i + 1].transform.position.z - floorPrefab.GetComponent<Renderer>().bounds.size.z);
                platforms.Add(FloorPlace);
                platforms.Add(FloorPlace2);

                temp_waypoint.GetComponent<Waypoint>().direction = directions[i];
            }
            //Destroy platforms colliding with the path floor
            for (int i = 0; i < platforms.Count; i++) //TODO: fix platform generation, so no destroy is needed
            {
                for (int j = 0; j < path.Count; j++)
                {
                    if (Mathf.Abs(platforms[i].transform.position.x - path[j].transform.position.x) < 1f && Mathf.Abs(platforms[i].transform.position.z - path[j].transform.position.z) < 1f)
                    {
                        Destroy(platforms[i]);
                        platforms.Remove(platforms[i]);
                        i -= 1;
                    }
                }
            }
            //Destory platforms colliding with another platform
            for (int i = 0; i < platforms.Count; i++) //TODO: fix platform generation, so no destroy is needed
            {
                for (int j = 0; j < platforms.Count; j++)
                {
                    if (j != i)
                    {
                        if (Mathf.Abs(platforms[i].transform.position.x - platforms[j].transform.position.x) < 1f && Mathf.Abs(platforms[i].transform.position.z - platforms[j].transform.position.z) < 1f)
                        {
                            Destroy(platforms[j]);
                            platforms.Remove(platforms[j]);
                        }
                    }
                }
            }
            break;
        }
    }

    List<int> waypointDirections = new List<int>();

    private void GeneratePath()
    {
        int wantedBlocks = 200;
        int verticalBlocks = wantedBlocks;

        if (verticalBlocks % 2 == 1)
        {
            verticalBlocks -= 1;
        }

        int heightsCombined = wantedBlocks / 2;

        List<int> branchHeights = new List<int>();

        path.Add(Instantiate(floorPrefab, new Vector3(0, 0, 0), Quaternion.identity, transform));
        AddBlockRight();

        while (true)
        {
            int branchHeight = Random.Range(4, (int)gridSize.y);
            if (heightsCombined - branchHeight > 0)
            {
                heightsCombined -= branchHeight;
                branchHeights.Add(branchHeight);
            }
            else
            {
                branchHeights.Add(heightsCombined);
                break;
            }
        }

        int vertBuilds = 0;

        while (wantedBlocks > 0)
        {
            if (vertBuilds < branchHeights.Count)
            {
                for (int i = 0; i < branchHeights[vertBuilds]; i++)
                {
                    AddBlockUp();
                    wantedBlocks--;
                }
                for (int i = 0; i < 3; i++)
                {
                    AddBlockRight();
                    wantedBlocks--;
                }
                for (int i = 0; i < branchHeights[vertBuilds]; i++)
                {
                    AddBlockDown();
                    wantedBlocks--;
                }
            }
            vertBuilds++;
            if (vertBuilds < branchHeights.Count)
            {
                for (int i = 0; i < branchHeights[vertBuilds]; i++)
                {
                    AddBlockDown();
                    wantedBlocks--;
                }
                for (int i = 0; i < 3; i++)
                {
                    AddBlockRight();
                    wantedBlocks--;
                }
                for (int i = 0; i < branchHeights[vertBuilds]; i++)
                {
                    AddBlockUp();
                    wantedBlocks--;
                }
            }
            vertBuilds++;
        }

        List<Vector3> checkDirections = new List<Vector3>();
        checkDirections.Add(new Vector3(1, 0, 0));
        checkDirections.Add(new Vector3(-1, 0, 0));
        checkDirections.Add(new Vector3(0, 0, 1));
        checkDirections.Add(new Vector3(0, 0, -1));
        checkDirections.Add(new Vector3(1, 0, 1));
        checkDirections.Add(new Vector3(1, 0, -1));
        checkDirections.Add(new Vector3(-1, 0, 1));
        checkDirections.Add(new Vector3(-1, 0, -1));

        List<Vector3> emptyPositions = new List<Vector3>();
        Vector3 potentialPlatformPosition = new Vector3();
        GameObject temp_waypoint = new GameObject();

        for (int i = 0; i < path.Count; i++)
        {
            for (int j = 0; j < checkDirections.Count; j++)
            {
                potentialPlatformPosition = path[i].transform.position + checkDirections[j];
                if (!emptyPositions.Contains(potentialPlatformPosition) && CheckIfNoPathOnPosition(potentialPlatformPosition, i))
                {
                    emptyPositions.Add(potentialPlatformPosition);
                }
            }
            if (i < path.Count - 1)
            {
                temp_waypoint = Instantiate(waypointPrefab, path[i + 1].transform.position, Quaternion.identity, path[i + 1].transform);
                temp_waypoint.transform.localScale = new Vector3(0.1f, 1, 0.1f);
                temp_waypoint.GetComponent<Waypoint>().direction = waypointDirections[i];
            }
        }

        GameObject platform;

        for (int i = 0; i < emptyPositions.Count; i++)
        {
            platform = Instantiate(platformPrefab, path[0].transform.position, Quaternion.identity, transform);
            platform.transform.position = emptyPositions[i];
            platforms.Add(platform);
        }


    }

    private bool CheckIfNoPathOnPosition(Vector3 position, int currentTileID)
    {
        int tempTileID = currentTileID;
        if (tempTileID < 3)
        {
            tempTileID = 3;
        }
        else if(tempTileID > path.Count - 3)
        {
            tempTileID = path.Count - 3;
        }
        for (int i = tempTileID - 3; i < tempTileID + 3; i++)
        {
            if (path[i].transform.position == position)
            {
                return false;
            }
        }
        return true;
    }

    private void AddBlockRight()
    {
        GameObject floorRight = Instantiate(floorPrefab, path[path.Count - 1].transform.position + new Vector3(floorPrefab.GetComponent<Renderer>().bounds.size.x, 0, 0), Quaternion.identity, transform);
        waypointDirections.Add(0);
        path.Add(floorRight);
    }

    private void AddBlockUp()
    {
        GameObject floor = Instantiate(floorPrefab, path[path.Count - 1].transform.position + new Vector3(0, 0, floorPrefab.GetComponent<Renderer>().bounds.size.z), Quaternion.identity, transform);
        waypointDirections.Add(-1);
        path.Add(floor);
    }

    private void AddBlockDown()
    {
        GameObject floor = Instantiate(floorPrefab, path[path.Count - 1].transform.position - new Vector3(0, 0, floorPrefab.GetComponent<Renderer>().bounds.size.z), Quaternion.identity, transform);
        waypointDirections.Add(1);
        path.Add(floor);
    }

    private void AddPlatformLeft()
    {
        GameObject platform = Instantiate(platformPrefab, path[path.Count - 1].transform.position, Quaternion.identity, transform);
        platform.transform.position = path[path.Count - 1].transform.position + new Vector3(-1, 0, 0);
        platforms.Add(platform);
    }

    private void AddPlatformRight()
    {
        GameObject platform = Instantiate(platformPrefab, path[path.Count - 1].transform.position, Quaternion.identity, transform);
        platform.transform.position = path[path.Count - 1].transform.position + new Vector3(1, 0, 0);
        platforms.Add(platform);
    }

    private void AddPlatformUp()
    {
        GameObject platform = Instantiate(platformPrefab, path[path.Count - 1].transform.position, Quaternion.identity, transform);
        platform.transform.position = path[path.Count - 1].transform.position + new Vector3(0, 0, 1);
        platforms.Add(platform);
    }

    private void AddPlatformDown()
    {
        GameObject platform = Instantiate(platformPrefab, path[path.Count - 1].transform.position, Quaternion.identity, transform);
        platform.transform.position = path[path.Count - 1].transform.position + new Vector3(0, 0, -1);
        platforms.Add(platform);
    }

}

/*RaycastHit hit;
        GameObject platform = new GameObject();

        GameObject temp_waypoint = new GameObject();
        temp_waypoint.transform.localScale = new Vector3(0.1f, 1, 0.1f);

        for (int i = 0; i<path.Count; i++)
        {
            for(int j = 0; j< rayDirections.Count; j++)
            {
                if (!Physics.Raycast(path[i].transform.position, rayDirections[j], out hit, 1f))
                {
                    platform = Instantiate(platformPrefab, path[i].transform.position, Quaternion.identity, transform);
                    platform.transform.position = path[i].transform.position + rayDirections[j];
                    platforms.Add(platform);
                }
            }
            
        }*/
