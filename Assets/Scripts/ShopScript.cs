﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopScript : MonoBehaviour
{
    [SerializeField] private TurretPlacing turretPlacingScript;

    public void SetCurrentTurret(TurretObject newCurrentTurret)
    {
        turretPlacingScript.SetCurrentTurret(newCurrentTurret);
    }
}
