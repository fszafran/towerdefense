﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretPlacing : MonoBehaviour
{
    public Color highlightColor;
    public TurretObject currentTurret;

    [SerializeField] private PathGenerator path;
    [SerializeField] private MaterialManager mats;
    [SerializeField] private TurretUpgradePanel turretUpgradePanel;
    [SerializeField] private FactoryUpgradePanel factoryUpgradePanel;
    private GameObject hoveredPlatform;

    private void Update()
    {
        if (GetPlatformUnderMouse() != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                AttemptToPlaceTurret();
            }
        }
    }

    public void SetCurrentTurret(TurretObject item)
    {
        currentTurret = item;
        Debug.Log(item.name);
    }

    private void AttemptToPlaceTurret()
    {
        GameObject platformObject = GetPlatformUnderMouse();
        TurretPlatform turretPlatform = platformObject.GetComponent<TurretPlatform>();

        if (currentTurret != null)
        {
            if (!turretPlatform.CheckForTurret() && !turretUpgradePanel.CheckIfOpen() && !factoryUpgradePanel.CheckIfOpen())
            {
                if (mats.Pay(currentTurret.placePrices))
                {
                    GameObject turret = Instantiate(currentTurret.turretModel, platformObject.transform.position - new Vector3(0, 0, .2f), Quaternion.identity, transform);
                    turretPlatform.PlaceTurret(turret);
                }
            }
            else
            {
                turretUpgradePanel.SetNewCurrentTurret(turretPlatform.GetOwnedTurret(), PanelStatus.Open);
            }
        }
    }

    private GameObject GetPlatformUnderMouse()
    {
        hoveredPlatform?.GetComponent<TurretPlatform>().SetToDefaultColor();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.GetComponent<TurretPlatform>())
            {
                hoveredPlatform = hit.collider.gameObject;
                hoveredPlatform.GetComponent<TurretPlatform>().SetTemporaryColor(highlightColor);
                return hoveredPlatform;
            }
        }
        return null;
    }
}
