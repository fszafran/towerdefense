﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryLocker : MonoBehaviour
{
    public GameObject factoryModel;
    public bool doLock = false;

    private BoxCollider factoryCollider;
    
    private void Start()
    {
        factoryCollider = GetComponent<BoxCollider>();
        if(doLock)
        {
            Lock();
        }
    }

    public void Lock()
    {
        factoryCollider.enabled = false;
    }
    public void Unlock()
    {
        factoryCollider.enabled = true;
    }
}
