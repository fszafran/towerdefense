﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallHealth : MonoBehaviour
{
    public int maxHealthPoints;
    [HideInInspector] public int healthPoints;
    
    [SerializeField] private Slider healthSlider;

    private void Awake()
    {
        healthPoints = maxHealthPoints;
    }

    public void TakeDamage(int damage)
    {
        healthPoints -= damage;
        healthSlider.value = (float)healthPoints / (float)maxHealthPoints;
        if(healthPoints <= 0)
        {
            Debug.Log("Game Over");
        }
    }
}
