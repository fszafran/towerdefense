﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoriesManager : MonoBehaviour
{
    public FactoryProduction baseFactory;

    [SerializeField] private FactoryUpgradePanel factoryUpgradePanel;
    private FactoryProduction currentFactory;
    
    private void Start()
    {
        currentFactory = baseFactory;
        factoryUpgradePanel.UpdateCurrentFactory(currentFactory);
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if(SetCurrentFactoryIfClicked())
            {
                factoryUpgradePanel.UpdateCurrentFactory(currentFactory, PanelStatus.Open);
            }
        }
    }

    public FactoryProduction GetCurrentFactory()
    {
        return currentFactory;
    }

    private bool SetCurrentFactoryIfClicked()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray,out hit))
        {
            if (hit.collider.CompareTag("Factory"))
            {
                currentFactory = hit.collider.GetComponent<FactoryProduction>();
                return true;
            }
        }
        return false;
    }
}
