﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardScript : MonoBehaviour
{
    [SerializeField] private float desiredZRotation;

    private Transform cameraTransform;

    private void Start()
    {
        cameraTransform = Camera.main.transform;
    }

    private void LateUpdate()
    {
        transform.LookAt(transform.position + cameraTransform.forward);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, desiredZRotation);
    }
}
