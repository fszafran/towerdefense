﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum MaterialType
{
    Wood = 0,
    Stone,
    Iron,
    Gold
}

public class MaterialManager : Singleton<MaterialManager>
{
    //public static MaterialManager Instance { get; private set; }


    public List<MaterialObject> mats;

    [SerializeField] private GameObject materialHUDPrefab;
    [SerializeField] private GameObject materialCanvasTransform;

    /*override private void Awake()
    {
        base.Awake();
    }*/

    private void Start()
    {
        for (int i = 0; i < mats.Count; i++)
        {
            GameObject mat = Instantiate(materialHUDPrefab, materialCanvasTransform.transform);
            mat.GetComponent<MaterialHUD>().mat = mats[i];
        }
    }

    public bool Pay(Price turretCost)
    {
        for(int i = 0; i<mats.Count; i++)
        {
            if (mats[i].materialType == turretCost.materialType)
            {
                mats[i].amount -= turretCost.cost;
                return true;
            }
        }
        return false;
    }

    public bool Pay(List<Price> turretCosts)
    {
        if (CheckIfEnoughMatsToPay(turretCosts))
        {
            for (int i = 0; i < turretCosts.Count; i++)
            {
                for (int j = 0; j < mats.Count; j++)
                {
                    if (turretCosts[i].materialType == mats[j].materialType)
                    {
                        mats[j].amount -= turretCosts[i].cost;
                    }
                }
            }
            return true;
        }
        return false;
    }
    public MaterialObject GetMaterial(MaterialType materialType)
    {
        foreach(MaterialObject material in mats)
        {
            if(material.materialType == materialType)
            {
                return material;
            }
        }
        return null;
    }

    private bool CheckIfEnoughMatsToPay(List<Price> turretCosts)
    {
        for (int i = 0; i < turretCosts.Count; i++)
        {
            for (int j = 0; j < mats.Count; j++)
            {
                if (turretCosts[i].materialType == mats[j].materialType)
                {
                    if (mats[j].amount < turretCosts[i].cost)
                        return false;
                }
            }
        }
        return true;
    }
}