﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnEnemies : MonoBehaviour
{
    public static SpawnEnemies Instance { get; private set; }

    public int waveSize = 2;
    public List<GameObject> enemies = new List<GameObject>();

    [SerializeField] private PathGenerator path;
    [SerializeField] private Object enemyPrefab;
    [SerializeField] private GameObject waveButton;
    private int spawnedEnemies = 0;
    private Vector3 spawnPosition;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        if (path != null)
        {
            spawnPosition = path.path[path.path.Count - 1].transform.position + new Vector3(0, .5f, 0);
        }
        else
        {
            spawnPosition = new Vector3(0, 0, 0);
        }
        StartCoroutine(SpawnOnTime(1));
    }

    private void Update()
    {
        if (enemies.Count == 0 && spawnedEnemies == waveSize)
        {
            waveButton.SetActive(true);
        }
        foreach(GameObject enemy in enemies)
        {
            if (enemy.GetComponent<Enemy>().healthPoints <= 0)
            {
                enemies.Remove(enemy);
                Destroy(enemy);
                break;
            }
        }
    }

    public void Construct(Object _enemyPrefab)
    {
        enemyPrefab = _enemyPrefab;
    }

    public void StartNewWave()
    {
        StartCoroutine(SpawnOnTime(1));
        waveSize *= 2;
    }

    private IEnumerator SpawnOnTime(float time)
    {
        spawnedEnemies = 0;
        while (spawnedEnemies < waveSize)
        {
            GameObject enemy = Instantiate(enemyPrefab, spawnPosition, Quaternion.identity, transform) as GameObject;
            enemies.Add(enemy);
            spawnedEnemies++;
            yield return new WaitForSeconds(time);
        }
        yield break;
    }
}
