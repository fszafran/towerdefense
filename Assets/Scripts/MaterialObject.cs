﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "New Material", menuName = "Material Object")]
public class MaterialObject : ScriptableObject
{
    public int amount;
    public MaterialType materialType;
    public Sprite materialIcon;
}
