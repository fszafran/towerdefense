﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEditor;

namespace Tests
{
    public class TestEnemy
    {
        [UnityTest]
        public IEnumerator Change_enemy_direction()
        {
            var enemyPrefab = Resources.Load("Prefabs/GameObjects/EnemyPrefab");
            var enemySpawner = new GameObject().AddComponent<SpawnEnemies>();
            enemySpawner.Construct(enemyPrefab);


            yield return new WaitForSeconds(2);

            var spawnedEnemy = GameObject.FindWithTag("Enemy");

            var waypoint = new GameObject().AddComponent<Waypoint>();
            waypoint.gameObject.AddComponent<BoxCollider>();
            waypoint.GetComponent<BoxCollider>().size = new Vector3(10, 10, 10);
            waypoint.direction = 1;
            waypoint.gameObject.transform.position = spawnedEnemy.transform.position;

            yield return null;

            Assert.AreEqual(spawnedEnemy.GetComponent<Enemy>().direction, 1);
        }
    }
}
